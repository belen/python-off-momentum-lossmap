'''
AppOffMomentumLossmap -- A python GUI to control and monitor LHC off-momentum loss maps
Belen Salvachua - Aug 2018
'''

from PyQt5 import QtCore, QtGui, QtWidgets
from gui_offmomentum_ui import Ui_MainWindow
from off_momentum_lossmap import offMomentumLossmap
from generic_monitoring import RfFreqMonitoring,BlmB1Monitoring,BlmB2Monitoring,BctMonitoring
import sys
import pyjapc

import datetime
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import threading
import numpy as np
import pyqtgraph as pg

import pg_time_axis as pgt

class AppMain(Ui_MainWindow):
    def __init__(self):

        today_date = datetime.datetime.now()
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        pg.setConfigOptions(antialias=True)
        app = QtWidgets.QApplication(sys.argv)
        MainWindow = QtWidgets.QMainWindow()
        
        super(AppMain,self)
        self.setupUi(MainWindow)

        # Configure JAPC
        self.japc = pyjapc.PyJapc( noSet = True )
        self.japc.setSelector(None)
        self.japc.rbacLogin()
        #self.japc.rbacLogin(loginDialog=True )


        # Initialize offMomentumLossmap object
        self._lm = offMomentumLossmap(self.japc)        
        self._lm.newMSGready.connect(self.updateTextMSG)        

        # Initialize OffMomentumMonitoring object (just used for the plot)
        self._monitorBlmB1 = BlmB1Monitoring(self.japc)
        self._monitorBlmB1.newDataReceived.connect(self.updateBlmB1Plot)
        self._monitorBlmB2 = BlmB2Monitoring(self.japc)
        self._monitorBlmB2.newDataReceived.connect(self.updateBlmB2Plot)
        self._monitorRf = RfFreqMonitoring(self.japc)
        self._monitorRf.newDataReceived.connect(self.updateRfPlot)
        self._monitorBct = BctMonitoring(self.japc)
        self._monitorBct.newDataReceived.connect(self.updateBctPlot)
        
        # Connect all the buttons with the GUI
        self.pushButton_loop_neg_dp.clicked.connect(self.startThread_freq_loop_neg_dp)
        self.pushButton_loop_pos_dp.clicked.connect(self.startThread_freq_loop_pos_dp)
        self.pushButton_fb_1Hz_neg_dp.clicked.connect(self.startThread_freq_fb_neg_dp)
        self.pushButton_fb_1Hz_pos_dp.clicked.connect(self.startThread_freq_fb_pos_dp)
        self.pushButton_fb_100Hz_neg_dp.clicked.connect(self.startThread_freq_fb_100Hz_neg_dp)
        self.pushButton_fb_100Hz_pos_dp.clicked.connect(self.startThread_freq_fb_100Hz_pos_dp)
        self.pushButton_set_injection_default.clicked.connect(self.set_injection_default)
        self.pushButton_set_top_default.clicked.connect(self.set_top_default)
        
        # Connect STOP LOSS MAP button 
        self.pushButton_stop_lossmap.clicked.connect(self.send_stop_lossmap_command)

        # Set RF frequence limits from default values
        self.get_default_rf_freq_settings()

        # Set Loss maps limtis from default values
        self.get_default_lossmap_limit()

        # Connect GET default RF frequence from Loss map class
        self.pushButton_rf_freq_default.clicked.connect(self.get_default_rf_freq_settings)
        self.pushButton_rf_freq_get.clicked.connect(self.get_rf_freq_settings)
        self.pushButton_rf_freq_set.clicked.connect(self.set_rf_freq_settings)

        # Connect GET default loss map limits from Loss map Class
        self.pushButton_default_lossmap_limit.clicked.connect(self.get_default_lossmap_limit)
        self.pushButton_get_lossmap_limit.clicked.connect(self.get_lossmap_limit)
        self.pushButton_set_lossmap_limit.clicked.connect(self.set_lossmap_limit)
        
        # Start Subscriptions
        self._monitorBlmB1._japc.startSubscriptions()
        self._monitorBlmB2._japc.startSubscriptions()
        self._monitorRf._japc.startSubscriptions()
        self._monitorBct._japc.startSubscriptions()
        
        
        # Initialize plots
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        pg.setConfigOptions(antialias=True)

        # Time axis in plots
        axis_blm_b1 = pgt.DateAxisItem(orientation = 'bottom')
        axis_blm_b1.attachToPlotItem(self.graphicsView_blm_b1.getPlotItem())

        axis_blm_b2 = pgt.DateAxisItem(orientation = 'bottom')
        axis_blm_b2.attachToPlotItem(self.graphicsView_blm_b2.getPlotItem())
        
        axis_rf = pgt.DateAxisItem(orientation = 'bottom')
        axis_rf.attachToPlotItem(self.graphicsView_rf_freq.getPlotItem())

        axis_bct = pgt.DateAxisItem(orientation = 'bottom')
        axis_bct.attachToPlotItem(self.graphicsView_bct.getPlotItem())

        # Re-style the plots
        self.restylePlot(self.graphicsView_blm_b1,'Time from '+today_date.strftime("%Y-%m-%d"),None,'BLM signal','Gy/s', xrange=(0,self._monitorBlmB1._npoints), yrange=(1e-5,1))
        self.builtLegend(self.graphicsView_blm_b1, self._monitorBlmB1)

        self.restylePlot(self.graphicsView_blm_b2,'Time from '+today_date.strftime("%Y-%m-%d"),None,'BLM signal','Gy/s', xrange=(0,self._monitorBlmB2._npoints), yrange=(1e-5,1))
        self.builtLegend(self.graphicsView_blm_b2, self._monitorBlmB2)
        
        self.restylePlot(self.graphicsView_rf_freq,'Time from '+today_date.strftime("%Y-%m-%d"),None,'RF frequency','Hz')

        self.restylePlot(self.graphicsView_bct,'Time from '+today_date.strftime("%Y-%m-%d"),None,'Beam Curernt','charges')
        self.builtLegend( self.graphicsView_bct, self._monitorBct )

    
        # Start display
        MainWindow.show()
        sys.exit(app.exec_())


    def builtLegend(self, plot, monitor):
        legend = pg.LegendItem()
        keys = list(monitor.x_values.keys())
        plot_items = []
        count = -1
        for key in keys:
            count += 1
            plot_items += [ plot.plot(self.removeNansForPg(monitor.x_values[key]),
                                      self.removeNansForPg(monitor.y_values[key]),
                                      pen = pg.mkPen( width = 3, color= monitor.lineColors[count]),name = key, clear = (count==0)) ]
            legend.addItem(plot_items[count],key)
            legend.setParentItem(plot.getPlotItem())
            legend.anchor((1,0),(0.4,0))
     


        
        
    def restylePlot(self,plotId,botLabel,botUnit,leftLabel,leftUnit,xrange=None,yrange=None):
        ticks_font=QFont()
        ticks_font.setPixelSize(10)
        labelStyle={'font-size':'10pt'}  
        plotId.setLabel('left', leftLabel, units= leftUnit,**labelStyle)
        if botUnit is None:
            plotId.setLabel('bottom',botLabel,**labelStyle)                    
        else:               
            plotId.setLabel('bottom',botLabel,units=botUnit,**labelStyle)                    
        plotId.getAxis('bottom').setStyle(tickTextOffset=15)
        plotId.getAxis('left').tickFont=ticks_font
        plotId.getAxis('bottom').tickFont=ticks_font
        plotId.getAxis('bottom').enableAutoSIPrefix(False)
        plotId.getAxis('left').enableAutoSIPrefix(False)
        plotId.showGrid(x=True,y=True)
        #if xrange is not None:
        #    plotId.setXRange(xrange[0],xrange[1])
        #if yrange is not None:
        #    plotId.setYRange(yrange[0],yrange[1])

        
        
    def removeNansForPg(self,x):
        try:
            x=x[~np.isnan(x)] 
        except TypeError:
            pass
        return x

    def updatePlot( self, plot, monitor, xLog = False, yLog = False) :
        count = -1
        for key in monitor.x_values.keys():
            count += 1
            plot.plot(self.removeNansForPg(monitor.x_values[key]),
                      self.removeNansForPg(monitor.y_values[key]),
                      pen=pg.mkPen(width= 3, color= monitor.lineColors[count]),name = key, clear = (count==0))
        plot.setLogMode(xLog, yLog)
            
    def updateBlmB1Plot(self,textSignal):
        self.updatePlot(self.graphicsView_blm_b1, self._monitorBlmB1, False, True )

    def updateBlmB2Plot(self,textSignal):
        self.updatePlot(self.graphicsView_blm_b2, self._monitorBlmB2, False, True )

    def updateRfPlot(self,textSignal):
        self.updatePlot(self.graphicsView_rf_freq, self._monitorRf, False, False )

    def updateBctPlot(self,textSignal):
        self.updatePlot(self.graphicsView_bct, self._monitorBct, False, False )


    def set_injection_default(self):
        self._lm.rf_freq_stop  = 400
        self._lm.rf_freq_step  = 20
        self._lm.max_ratio_limit = 20
        self._lm.ds_relative_limit = 5
        self.get_rf_freq_settings()
        self.get_lossmap_limit()

    def set_top_default(self):
        self._lm.rf_freq_stop  = 200
        self._lm.rf_freq_step  = 5
        self._lm.max_ratio_limit = 20
        self._lm.ds_relative_limit = 10
        self.get_rf_freq_settings()
        self.get_lossmap_limit()
        
    # Function that enables to use threads within japc
    def func_japc_thread(self, target):
        self._lm._japc.enableInThisThread()
        try:
            target()
        except:
            self._lm.stop_freq_trim()
            print("Something unexpected: I REVERT FREQ. TRIM ")
            # self.updateTextMSG("Something unexpected: I REVERT FREQ. TRIM ")
            # (Make sure 'QTextBlock' is registered using qRegisterMetaType().)


    # Generic method to start the thread
    def start_button_thread(self, targetFunc ):
        try:
            if self.t.is_alive() == True:
                self.updateTextMSG("One thread is still already alive: try later")
                return
        except AttributeError:
            pass                
        self.t=threading.Thread( target = self.func_japc_thread, args = (targetFunc,))
        #self.t.daemon = True
        self.t.start()
            
    @pyqtSlot()
    def startThread_freq_loop_neg_dp(self): 
        self.start_button_thread( self._lm.lossmap_freq_loop_neg_dp )

    @pyqtSlot()
    def startThread_freq_loop_pos_dp(self): 
        self.start_button_thread( self._lm.lossmap_freq_loop_pos_dp )

    @pyqtSlot()
    def startThread_freq_fb_neg_dp(self): 
        self.start_button_thread( self._lm.lossmap_freq_fb_neg_dp )

    @pyqtSlot()
    def startThread_freq_fb_pos_dp(self): 
        self.start_button_thread( self._lm.lossmap_freq_fb_pos_dp )

    @pyqtSlot()
    def startThread_freq_fb_100Hz_neg_dp(self): 
        self.start_button_thread( self._lm.lossmap_freq_fb_100Hz_neg_dp )

    @pyqtSlot()
    def startThread_freq_fb_100Hz_pos_dp(self): 
        self.start_button_thread( self._lm.lossmap_freq_fb_100Hz_pos_dp )
    
    # Function to send text to the textBrowser
    @pyqtSlot(str)   
    def updateTextMSG(self,textMSG):
        self.textBrowser.insertPlainText(textMSG)
        self.textBrowser.moveCursor(QtGui.QTextCursor.End)
        self.textBrowser.ensureCursorVisible()   
        
    @pyqtSlot()
    def send_stop_lossmap_command(self):
        self._lm.stop_freq_trim()
        self._lm.interruptLoop = True
        
    @pyqtSlot()
    def get_default_rf_freq_settings(self):
        #self.lineEdit_rf_freq_start.setText("%4.0f" %(self._lm._start_freq_default))
        self.lineEdit_rf_freq_stop.setText("%4.0f" %(self._lm._stop_freq_default))
        self.lineEdit_rf_freq_step.setText("%4.0f" %(self._lm._step_freq_default))
        self.updateTextMSG("Setting RF freq limits (start,stop,step): %3.0f Hz, %3.0f Hz, %3.0f Hz\n" %(self._lm._start_freq_default,self._lm._stop_freq_default,self._lm._step_freq_default))
    @pyqtSlot()
    def get_rf_freq_settings(self):
        #self.lineEdit_rf_freq_start.setText("%4.0f" %(self._lm.rf_freq_start))
        self.lineEdit_rf_freq_stop.setText("%4.0f" %(self._lm.rf_freq_stop))
        self.lineEdit_rf_freq_step.setText("%4.0f" %(self._lm.rf_freq_step))
        self.updateTextMSG("Setting RF freq limits (start,stop,step): %3.0f Hz, %3.0f Hz, %3.0f Hz\n" %(self._lm.rf_freq_start,self._lm.rf_freq_stop,self._lm.rf_freq_step))
        
    @pyqtSlot()
    def set_rf_freq_settings(self):
        try:
            #start  = self.lineEdit_rf_freq_start.text()
            stop   = self.lineEdit_rf_freq_stop.text()
            step   = self.lineEdit_rf_freq_step.text() 
            #self._lm.rf_freq_start = int(start )
            self._lm.rf_freq_stop  = int(stop)
            self._lm.rf_freq_step  = int(step)
        except ValueError:
            self.updateTextMSG("\nValueError: Incorrect (start,stop,step) -> (%s,%s,%s)\n" %(start,stop,step))
            self.get_rf_freq_settings()
            pass            
        self.updateTextMSG("Setting RF freq limits (start,stop,step): %3.0f Hz, %3.0f Hz, %3.0f Hz\n" %(self._lm.rf_freq_start,self._lm.rf_freq_stop,self._lm.rf_freq_step))

    @pyqtSlot()
    def get_default_lossmap_limit(self):
        self.lineEdit_dump_limit.setText("%4.0f" %(self._lm._max_ratio_limit_default))
        self.lineEdit_ip3_ds_limit.setText("%4.0f" %(self._lm._ds_relative_limit_default))
        self.updateTextMSG("Setting Loss map limits (Max.Thresdhold to dump, IP3 DS/DS_init): %3.0f %%, %3.0f \n" %(self._lm._max_ratio_limit_default,self._lm._ds_relative_limit_default))
        
    @pyqtSlot()
    def get_lossmap_limit(self):
        self.lineEdit_dump_limit.setText("%4.0f" %(self._lm.max_ratio_limit))
        self.lineEdit_ip3_ds_limit.setText("%4.0f" %(self._lm.ds_relative_limit))
        self.updateTextMSG("Setting Loss map limits (Max.Thresdhold to dump, IP3 DS/DS_init): %3.0f %%, %3.0f \n" %(self._lm.max_ratio_limit,self._lm.ds_relative_limit))

        
    @pyqtSlot()
    def set_lossmap_limit(self):
        try:
            dump_limit = self.lineEdit_dump_limit.text()
            ds_ds_init = self.lineEdit_ip3_ds_limit.text()
            self._lm.max_ratio_limit = int(dump_limit)
            self._lm.ds_relative_limit = int(ds_ds_init)
        except ValueError:
            self.updateTextMSG("\nValueError: Incorrect (Max.Thresdhold to dump, IP3 DS/DS_init) -> %3.0f %%, %3.0f \n" %(dump_limit,ds_ds_init))
            self.get_lossmap_limit()
            pass            
        self.updateTextMSG("Setting Loss map limits (Max.Thresdhold to dump, IP3 DS/DS_init): %3.0f %%, %3.0f \n" %(self._lm.max_ratio_limit,self._lm.ds_relative_limit))


                           
AppMain()

