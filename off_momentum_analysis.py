import pytimber as pt
import matplotlib.pyplot as plt
import numpy as np
import pyjapc

variableName = "BLMTI.04R1.B2I10_TCTPV.4R1.B2"
#variableName = "BLMTI.06L7.B1E10_TCP.B6L7.B1"
#variableName = "BLMTI.06R3.B2E10_TCP.6R3.B2"
starTime = "2018-09-23 06:49:00"
endTime  = "2018-09-23 06:51:00"

variableName = "BLMTI.06L7.B1E10_TCP.C6L7.B1"
starTime = "2018-09-13 15:40:00"
endTime  = "2018-09-13 16:35:00"


variableName100Hz = variableName+":LOSS_FAST"
variableNameThresRS06 = variableName + ":THRESH_RS06"

db = pt.LoggingDB(source="mdb")
data100Hz = db.get( variableName100Hz , starTime, endTime )
dataThreshold = db.getScaled( variableNameThresRS06, starTime , endTime, scaleAlgorithm = "REPEAT")

data100Hz_uncompressed = []
for value_sec in data100Hz[variableName100Hz][1]:
    for value_10msec in value_sec:
        data100Hz_uncompressed += [ value_10msec ] 

def return_blm_index(filter_names, blm_values_all_names):
    for name in filter_names:
        print(name)
        for i in range(len(blm_values_all_names)):
            if name in blm_values_all_names[i] and "BLMTI" in blm_values_all_names[i]:
                print(blm_values_all_names[i])
                yield i

#########################
japc = pyjapc.PyJapc( noSet = True )
japc.rbacLogin()
blm_thres = japc.getParam("LHC_BLM_ACQ/Acquisition#thresholds")
blm_expert_name = japc.getParam("LHC_BLM_ACQ/Acquisition#expertMonitorNames")
blm_rs06 = [ blm[5] for blm in blm_thres ]
blm_coll_index  = list( return_blm_index([variableName.split("_")[1]], blm_expert_name ) )
print(blm_coll_index)
print(blm_rs06[blm_coll_index[0]])


plt.figure()
plt.title(variableName100Hz)
plt.plot( np.arange(0, len(data100Hz_uncompressed))*0.01, data100Hz_uncompressed )
plt.hlines(dataThreshold[variableNameThresRS06][1][0], 0, len(data100Hz_uncompressed)*0.01, colors="black")
plt.ylabel("BLM signal (Gy/s)")
plt.xlabel("Time (s) from ["+starTime+"]")
plt.yscale('log')

plt.figure()
plt.title(variableName100Hz)
plt.plot( np.arange(0, len(data100Hz_uncompressed))*0.01, data100Hz_uncompressed/dataThreshold[variableNameThresRS06][1][0]*100. )
plt.hlines(100., 0, len(data100Hz_uncompressed)*0.01, colors="red")
plt.hlines(20., 0, len(data100Hz_uncompressed)*0.01, colors="green")
plt.ylabel("BLM signal to Dump (%)")
plt.xlabel("Time (s) from ["+starTime+"]")
plt.yscale('log')
plt.show()
