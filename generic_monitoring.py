# run first this
#source /user/lhcop/profiles/python.sh
import pyjapc
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import numpy as np
import datetime, time, pytz
from dateutil import tz

        
class GenericMonitoring(QObject):
    newDataReceived = pyqtSignal(str)

    def __init__(self, japc = None, fesaProperties = None, lineNames = None, npoints = 3600):
        super(GenericMonitoring,self).__init__()
        
        if japc is None:
            self._japc = pyjapc.PyJapc( noSet = True )
            self._japc.setSelector(None) # This is needed for the RF
            self._japc.rbacLogin()
        else:
            self._japc = japc

        if fesaProperties is None:
            return
        self.fesaProperties = fesaProperties
        self.signalName = "default"
        
        self.lineNames = lineNames
        if self.lineNames == None:
            self.lineNames = []
            for name in self.fesaProperties:
                self.lineNames += [name]
            
        self._npoints = npoints
        self.counter = {}
        self.y_values = {}
        self.x_values = {}
        for name in self.lineNames:
            self.counter[name] = 0
            self.y_values[name] = []
            self.x_values[name] = []
            for i in range(self._npoints):
                self.y_values[name].append(np.nan)
                self.x_values[name].append(np.nan)
               
        for cmd in self.fesaProperties:
            self._japc.subscribeParam( cmd, self.onValueReceived , getHeader=True, unixtime = False)

        
            
    def close(self):
        self._japc.stopSubscriptions()
        self._japc.clearSubscriptions()
        print("\nSayonara...")
        
    def __del__(self):
        self.close()
        
    def onValueReceived(self, japc_cmd, value, headerInfo):

        currentDatetimeUTC = headerInfo['acqStamp']
        currentDatetime = currentDatetimeUTC.astimezone( tz.tzlocal() )
        currentTimestamp = time.mktime( currentDatetime.timetuple() )
        name = self.lineNames[ (self.fesaProperties).index(japc_cmd) ]
        
        if self.counter[name] >= len( self.y_values[name]) :
            self.y_values[name].pop(0) 
            self.y_values[name].append( value)
            self.x_values[name].pop(0)
            self.x_values[name].append( currentTimestamp )
        else:
            self.y_values[name][self.counter[name]] = value
            self.x_values[name][ self.counter[name] ] = currentTimestamp

        self.counter[name] += 1
        self.newDataReceived.emit(self.signalName)
 
class BlmB1Monitoring(GenericMonitoring):
    def __init__(self, japc = None):
        partial_fesa_command = "_Align/FastBLMAvg#FastBLMAvgData"
        line_names = ("TCP.B6L7.B1", "TCP.6L3.B1", "IR3_DS_B1")
        this_fesa_properties = [ name+"_Align/FastBLMAvg#FastBLMAvgData" for name in line_names ]
        super(BlmB1Monitoring,self).__init__(fesaProperties = this_fesa_properties, lineNames = line_names)
        self.lineColors = ( (0,0,204),
                            (204,0,0),
                            (255,0,255))
        self.signalName = "BLMB1"

class BlmB2Monitoring(GenericMonitoring):
    def __init__(self, japc = None):
        partial_fesa_command = "_Align/FastBLMAvg#FastBLMAvgData"
        line_names = ("TCP.B6R7.B2", "TCP.6R3.B2", "IR3_DS_B2")
        this_fesa_properties = [ name+"_Align/FastBLMAvg#FastBLMAvgData" for name in line_names ]
        super(BlmB2Monitoring,self).__init__(fesaProperties = this_fesa_properties, lineNames = line_names)
        self.lineColors = ( (0,0,204),
                            (204,0,0),
                            (255,0,255))
        self.signalName = "BLMB2"

class RfFreqMonitoring(GenericMonitoring):
    def __init__(self, japc = None):
        this_fesa_properties = ("LHCALLRfFreqProg/Acquisition#freq1",)

        line_names = ("freq1",)
        super(RfFreqMonitoring,self).__init__(fesaProperties = this_fesa_properties, lineNames = line_names)
        self.lineColors = ( (0,0,255))
        self.signalName = "freq"
        self.firstFreq = self._japc.getParam(this_fesa_properties)[0]
        
    def onValueReceived(self, japc_cmd, value, headerInfo ):
        currentDatetimeUTC = headerInfo['acqStamp']
        currentDatetime = currentDatetimeUTC.astimezone( tz.tzlocal() )
        currentTimestamp = time.mktime( currentDatetime.timetuple() )
        name = self.lineNames[ (self.fesaProperties).index(japc_cmd) ]
        self.y_values[name] = [ value + self.firstFreq for value in self.y_values[name]]
        if self.counter[name] >= len( self.y_values[name]) :
            self.y_values[name].pop(0) 
            self.y_values[name].append( value)
            self.x_values[name].pop(0)
            self.x_values[name].append( currentTimestamp )
        else:
            self.y_values[name][self.counter[name]] = value
            self.x_values[name][ self.counter[name] ] = currentTimestamp
        self.y_values[name] = [ value - self.firstFreq for value in self.y_values[name]]
        self.counter[name] += 1
        self.newDataReceived.emit(self.signalName)

class BctMonitoring(GenericMonitoring):
    def __init__(self, japc = None):
        this_fesa_properties = ("LHC.BCTDC24.A/Acquisition#totalIntensityB1",
                                "LHC.BCTDC24.A/Acquisition#totalIntensityB2")
        line_names = ("Beam1","Beam2")
        super(BctMonitoring,self).__init__(fesaProperties = this_fesa_properties, lineNames = line_names)
        self.lineColors = ( (0,0,204),
                            (204,0,0))
        self.signalName = "BCT"
