#!/user/bdisoft/operational/bin/Python/PRO/bin/python

'''
off_momentum_lossmap -- A python Class to control LHC off-momentum loss maps
Belen Salvachua - Aug 2018
'''

# run first this
#source /user/lhcop/profiles/python.sh
from itertools import compress
import operator
import pyjapc
import time
from PyQt5.QtCore import *
from PyQt5.QtGui import *


from datetime import datetime
import threading

class offMomentumLossmap(QObject):
    newMSGready= pyqtSignal(str)    

    def __init__(self, japc = None):
        super(offMomentumLossmap,self).__init__()
        if japc is None:
            self._japc = pyjapc.PyJapc( noSet = True )
            #self._japc.setSelector("LHC.USER.ALL")
            self._japc.setSelector(None) # This is needed for the RF
            self._japc.rbacLogin()
            #self._japc.rbacLogin(loginDialog=True )
        else:
            self._japc = japc
        
        # Settings for the RF trims
        self._rt_rftrim_cmd = "LHC.BFSU/Settings#userRFTrim_dp"
        self._conversionFactor_Hz2dp = 8.3333e-6
        self._rt_rftrim_speed_Hz_sec = 110.0

        # Settings for the BLM values
        self._blm_values_cmd       = "LHC_BLM_ACQ/Acquisition#acq"
        self._blm_names_cmd        = "LHC_BLM_ACQ/Acquisition#monitorNames"
        self._blm_expert_names_cmd = "LHC_BLM_ACQ/Acquisition#expertMonitorNames"
        self._blm_thres_cmd        = "LHC_BLM_ACQ/Acquisition#thresholds"
        self._blm_masked_cmd       = "LHC_BLM_ACQ/Acquisition#masked"

        # Settings for the BCT values
        self._bct_b1_values_cmd       = "LHC.BCTDC24.A/Acquisition#totalIntensityB1"
        self._bct_b2_values_cmd       = "LHC.BCTDC24.A/Acquisition#totalIntensityB2"
        self._bct_b1_beam_presence_cmd= "LHC.BCTDC.A6R4.B1/Acquisition#arbiterFlag"
        self._bct_b2_beam_presence_cmd= "LHC.BCTDC.A6R4.B2/Acquisition#arbiterFlag"
        
        self._max_total_intensity_limit = 5e11
        
        # Settings for the collimators used and comparison limits
        #self._coll_names           = ("TCP.B6L7.B1","TCP.B6R7.B2")
        self._coll_names           = ("TCP.B6L7.B1","TCP.B6R7.B2","TCTPV.4R1.B2","TCTPV.4L5.B1" )
        self._ds_names             = ("8R3","8L3")
        self._ds_relative_limit_default    = 10
        self._max_ratio_limit_default      = 20
        self.ds_relative_limit    = self._ds_relative_limit_default
        self.max_ratio_limit      = self._max_ratio_limit_default
        #self.max_limit_100Hz       = 0.004 # new Gy/s RS06 INJECTION
        self.max_limit_100Hz       = 0.01 # new Gy/s RS06 TOP ENERGY

        # Interrupt the loop
        self.interruptLoop = False

        # Settings for the frequencies min,max, step
        self._start_freq_default = 0
        self._stop_freq_default = 201
        self._step_freq_default = 5
        self._absolute_max_freq = 500
        self.rf_freq_start = self._start_freq_default
        self.rf_freq_stop  = self._stop_freq_default
        self.rf_freq_step  = self._step_freq_default


        # Injection
        # FAST 100Hz
        # 400Hz / 40 Hz step
        # 20 % / 5 DS

        # Flat Top
        # 200Hz / 5 Hz step
        # 45% / 10 DS

        self._output_file_name = datetime.now().strftime("logger_off_momentum_lossmap_feedback_%Y_%m_%d_%H_%M_%S.log")
        self._output_file = open(self._output_file_name,"w")
        

    @property
    def max_ratio_limit(self):
        return self.__max_ratio_limit

    @max_ratio_limit.setter
    def max_ratio_limit(self, val):
        if val >= 90: self.__max_ratio_limit = 90
        elif val <= 0: self.__max_ratio_limit = 1
        else: self.__max_ratio_limit = val

    @property
    def ds_relative_limit(self):
        return self.__ds_relative_limit

    @ds_relative_limit.setter
    def ds_relative_limit(self, val):
        if val >= 50: self.__ds_relative_limit = 50
        elif val <= 0: self.__ds_relative_limit = 1
        else: self.__ds_relative_limit = val

    @property
    def rf_freq_stop(self):
        return self.__rf_freq_stop

    @rf_freq_stop.setter
    def rf_freq_stop(self, val):
        if abs(val) >  self._absolute_max_freq:
            self.printMSG("\nCannot start the loss map: max. frequency should be below %4.0f Hz\n" %(self._absolute_max_freq))
        else: self.__rf_freq_stop = val
        

    def printMSG(self,textMessage):
        msg_time = "["+str(datetime.now())+"]: "
        self.newMSGready.emit(msg_time+textMessage+"\n")                                                    
        print(msg_time+textMessage)
        self._output_file.write(msg_time+textMessage+"\n")
        self._output_file.flush()
        
    def start_freq_trim(self, freq_hz ):
        freq_dp = freq_hz*(-1.)*self._conversionFactor_Hz2dp
        self.printMSG( "Set RF freq trim of %5.1f Hz (%5.2g dp/p)" %(freq_hz , freq_dp))
        #value = self._japc.getParam(self._rt_rftrim_cmd)
        self._japc.setParam(self._rt_rftrim_cmd, freq_dp)
        
    def stop_freq_trim(self):
        #self._japc.getParam(self._rt_rftrim_cmd)
        self._japc.setParam(self._rt_rftrim_cmd, 0.0)
        self.printMSG("Revert RF freq trim to %5.1f Hz " %(0.0))

    def get_losses_init(self):
        blm_thres       = self._japc.getParam(self._blm_thres_cmd)
        blm_expert_name = self._japc.getParam(self._blm_expert_names_cmd)
        blm_masked      = self._japc.getParam(self._blm_masked_cmd)
        self._blm_not_masked  = list(map(operator.not_ , blm_masked))
        self._blm_thres       = list(compress(blm_thres,  self._blm_not_masked))
        self._blm_expert_name = list(compress(blm_expert_name, self._blm_not_masked))
        
    def check_losses_above_limit(self):
        blm_values      = self._japc.getParam(self._blm_values_cmd)
        blm_values      = list(compress(blm_values, self._blm_not_masked))
        self.blm_max_ratio_info    = (-1,-1,-1,"")
        for blm, thres, name in zip(blm_values, self._blm_thres, self._blm_expert_name):
            for irs in range( len(blm_values[0]) ):                
                ratio2Dump = blm[irs]/thres[irs]*100.0
                if ratio2Dump > self.blm_max_ratio_info[0] :
                    self.blm_max_ratio_info = (ratio2Dump, irs, blm[irs], name )
        if self.blm_max_ratio_info[0] > self.max_ratio_limit:
            self.printMSG("Loss %3.0f%% > %3.0f%% dump limit -> Loss map finished" %(self.blm_max_ratio_info[0], self.max_ratio_limit))
            return True
        return False

    def get_ds_init(self):
        blm_expert_name = self._japc.getParam(self._blm_expert_names_cmd)
        blm_values      = self._japc.getParam(self._blm_values_cmd)
        self._ds_index = list( self.return_blm_index(self._ds_names, blm_expert_name ) ) 
        self._ds_init_values = [ blm_values[index] for index in self._ds_index ]
        self._ds_init_names  = [ blm_expert_name[index] for index in self._ds_index ]
        
    def check_ds_above_limit(self):
        blm_values      = self._japc.getParam(self._blm_values_cmd)
        ds_values = [ blm_values[index] for index in self._ds_index ]
        for blm, blm_init in zip(ds_values, self._ds_init_values ):
            for irs in range(len(blm)):
                ds2ds_init = blm[irs]/blm_init[irs]
                if ds2ds_init > self.ds_relative_limit:
                    self.printMSG("DS/DS_init = %3.0f > %3.0f -> Loss map finished" %(ds2ds_init, self.ds_relative_limit))
                    return True
        return False

    def _check_beam_presence(self):
        b1_beam_presence = self._japc.getParam(self._bct_b1_beam_presence_cmd)
        b2_beam_presence = self._japc.getParam(self._bct_b2_beam_presence_cmd)
        if b1_beam_presence == False and b2_beam_presence == False:
            self.printMSG("Beam Presence for both beams is FALSE")
            return False
        return True

    
    # This is one algorithm to do the lossmap
    def lossmap_freq_loop(self, min_freq, max_freq, step_freq):
        self.printMSG("\n** Start Lossmap with LOOP RF frequency (min,max,step): %5.1f Hz , %5.1f Hz , %5.1f Hz **\n" %( min_freq, max_freq, step_freq))
        if self._check_bct_signal_above_limit() == True       : return
        if self._check_max_freq_above_limit(min_freq) == True : return
        if self._check_max_freq_above_limit(max_freq) == True : return                 
        self.get_ds_init()
        self.get_losses_init()
        
        second_time_trim = False
        freq_hz = min_freq - step_freq
        self.interruptLoop = False
        while abs(freq_hz) < abs(max_freq):
            if self.interruptLoop == True:
                self.printMSG("\n ** USER RF loss map interrupted, exiting **\n")
                self.stop_freq_trim()
                break

            if self._check_beam_presence() == False:
                self.stop_freq_trim() 
                break 

            freq_hz += step_freq
            self.start_freq_trim( freq_hz )
            freq_time_sec = abs(freq_hz/self._rt_rftrim_speed_Hz_sec)
            self.printMSG("Waiting %5.3fs RF freq changing" %(freq_time_sec))
            time.sleep( freq_time_sec ) 

  
            
            # Check losses and stop trim
            losses_above_limit = self.check_losses_above_limit()
            self.stop_freq_trim()
            ds_above_limit = self.check_ds_above_limit()

            # Take decision depening on the check
            if ds_above_limit:
                self.printMSG("DS/DS_init > %3.0f at %5.0f Hz -> Loss map finished" %(self.ds_relative_limit,freq_hz))
                break            
            elif losses_above_limit:
                self.print_blm_max_ratio()
                if second_time_trim:
                    self.print_blm_max_ratio()
                    self.printMSG("Frequency stopped : %5.0f Hz" %(freq_hz))
                    break
                self.printMSG("Trying same frequency 2nd time:  %5.0f Hz" %(freq_hz))
                second_time_trim = True
                freq_hz -= step_freq
        self.stop_freq_trim()

    def print_blm_max_ratio(self):
        self.printMSG("%s  RS%02d  %5.3g (Gy/s) : %5.3f %%" %(self.blm_max_ratio_info[3], self.blm_max_ratio_info[1], self.blm_max_ratio_info[2], self.blm_max_ratio_info[0]))
        return
    
    def lossmap_freq_fb(self, min_freq, max_freq, step_freq):
        self.printMSG("\n** Start Lossmap Feedback RF frequency (min,max,step): %5.1f Hz , %5.1f Hz , %5.1f Hz**\n" %( min_freq, max_freq, step_freq))
        if self._check_bct_signal_above_limit() == True       : return
        if self._check_max_freq_above_limit(min_freq) == True : return
        if self._check_max_freq_above_limit(max_freq) == True : return        
        self.get_ds_init()
        self.get_losses_init()
        self.interruptLoop = False
        for freq_hz in range(min_freq,max_freq,step_freq):
            if self.interruptLoop == True:
                self.printMSG("\n ** USER RF loss map interrupted, exiting **\n")
                self.stop_freq_trim()
                break
            if self._check_beam_presence() == False:
                self.stop_freq_trim() 
                break
            self.start_freq_trim( freq_hz )
            losses_above_limit = self.check_losses_above_limit()
            ds_above_limit     = self.check_ds_above_limit()
            if ds_above_limit:  
                self.printMSG("Frequency stopped : %5.0f Hz" %(freq_hz))
                break      
            elif losses_above_limit == True:
                self.printMSG("Frequency stopped : %5.0f Hz" %(freq_hz))
                self.print_blm_max_ratio()
                self.stop_freq_trim()
                break
        self.stop_freq_trim()
 
 
    def return_blm_index(self, filter_names, blm_values_all_names):
        for name in filter_names:
            for i in range(len(blm_values_all_names)):
                if name in blm_values_all_names[i] and "BLMTI" in blm_values_all_names[i]:
                    yield i

    def check_blm_100Hz_losses_above_limit(self):
        self._blm100Hz = []
        for name in self._coll_names:
            self._blm100Hz += [ self._japc.getParam(name+"_Align/FastBLM#FastBLMData")]
        for blm, thres in zip(self._blm100Hz , self._blm_thres_rs06):
            if blm > self.max_limit_100Hz:                
                self.printMSG("NEW Loss (100Hz) %5.3g Gy/s > %5.3g Gy/s limit-> Loss map finished" %( blm, self.max_limit_100Hz) )
                return True
            if blm/thres*100 > self.max_ratio_limit:
                self.printMSG("Loss (100Hz) %3.0f%% > %3.0f%% dump limit-> Loss map finished" %( blm/thres*100, self.max_ratio_limit))
                return True
        return False

    def _check_bct_signal_above_limit(self):
        b1_intensity = self._japc.getParam(self._bct_b1_values_cmd)
        b2_intensity = self._japc.getParam(self._bct_b2_values_cmd)
        if (b1_intensity > self._max_total_intensity_limit ) or (b2_intensity > self._max_total_intensity_limit ) :
            self.printMSG("Cannot start the loss maps: total intensity above %5.2g (protons)" %(self._max_total_intensity_limit))
            self.printMSG("Intensity: B1 = %5.2g (protons) B2 = %5.2g (protons)" %(b1_intensity,b2_intensity))
            return True
        return False

    def check_blm_100Hz_ds_above_limit(self):
        self._blm_ds_100Hz_b1 = self._japc.getParam("IR3_DS_B1_Align/FastBLM#FastBLMData")
        if self._blm_ds_100Hz_b1/self._blm_ds_100Hz_init_b1 > self.ds_relative_limit:
            self.printMSG("B1: DS/DS_init (100Hz) = %3.0f > %3.0f -> Loss map finished" %(self._blm_ds_100Hz_b1/self._blm_ds_100Hz_init_b1,self.ds_relative_limit))
            return True
        self._blm_ds_100Hz_b2 = self._japc.getParam("IR3_DS_B2_Align/FastBLM#FastBLMData")
        if self._blm_ds_100Hz_b2/self._blm_ds_100Hz_init_b2 > self.ds_relative_limit:
            self.printMSG("B2: DS/DS_init (100Hz) = %3.0f > %3.0f -> Loss map finished" %(self._blm_ds_100Hz_b2/self._blm_ds_100Hz_init_b2,self.ds_relative_limit))
            return True
        return False

    def _check_max_freq_above_limit(self, freq):
        if abs(freq) >= self._absolute_max_freq:
            self.printMSG("\nCannot start the loss map: max. frequency should be below %4.0f Hz\n" %(self._absolute_max_freq))
            return True
        return False

    def lossmap_freq_fb_100Hz(self, min_freq, max_freq, step_freq):
        self.printMSG("\n** Start Lossmap 100Hz Feedback RF frequency (min,max,step): %5.1f Hz , %5.1f Hz , %5.1f Hz**\n" %( min_freq, max_freq, step_freq))
        if self._check_bct_signal_above_limit() == True       : return
        if self._check_max_freq_above_limit(min_freq) == True : return
        if self._check_max_freq_above_limit(max_freq) == True : return
        blm_thres       = self._japc.getParam(self._blm_thres_cmd)
        blm_expert_name = self._japc.getParam(self._blm_expert_names_cmd)
        blm_coll_index  = list( self.return_blm_index(self._coll_names, blm_expert_name ) ) # BLMTI collimator  indexes 
        blm_thres_tmp = [ blm[5] for blm in blm_thres ] # RS06 threhodls for all
        self._blm_thres_rs06 = [ blm_thres_tmp[index] for index in blm_coll_index ] # RS06 thresholds for selected collimators
        self._blm_ds_100Hz_init_b1 = self._japc.getParam("IR3_DS_B1_Align/FastBLM#FastBLMData")
        self._blm_ds_100Hz_init_b2 = self._japc.getParam("IR3_DS_B2_Align/FastBLM#FastBLMData") 
        self.printMSG("\n** Start Lossmap (BLM 100Hz) Feedback RF frequency (min,max,step): %5.1f Hz , %5.1f Hz , %5.1f Hz**" %( min_freq, max_freq, step_freq))
        self.interruptLoop = False
        for freq_hz in range(min_freq,max_freq,step_freq):
            if self.interruptLoop == True:
                self.printMSG("\n ** USER RF loss map interrupted, exiting **\n")
                self.stop_freq_trim()
                break
            self.start_freq_trim( freq_hz )
            if self.check_blm_100Hz_ds_above_limit() == True:
                self.stop_freq_trim()
                self.printMSG("Frequency stopped : %5.0f Hz" %(freq_hz))
                break
            if self.check_blm_100Hz_losses_above_limit() == True:
                self.stop_freq_trim()
                self.printMSG("Frequency stopped : %5.0f Hz" %(freq_hz))
                break
        self.stop_freq_trim()


    def lossmap_freq_loop_pos_dp(self):
        self.lossmap_freq_loop( 0, -abs(self.rf_freq_stop), -abs(self.rf_freq_step) )
    def lossmap_freq_loop_neg_dp(self):
        self.lossmap_freq_loop( 0,  abs(self.rf_freq_stop), abs(self.rf_freq_step) )
    def lossmap_freq_fb_pos_dp( self ):
        self.lossmap_freq_fb( 0, -abs(self.rf_freq_stop), -abs(self.rf_freq_step) )
    def lossmap_freq_fb_neg_dp( self ):
        self.lossmap_freq_fb(  0,  abs(self.rf_freq_stop), abs(self.rf_freq_step) )
    def lossmap_freq_fb_100Hz_pos_dp( self ):
        self.lossmap_freq_fb_100Hz( 0, -abs(self.rf_freq_stop), -abs(self.rf_freq_step) )
    def lossmap_freq_fb_100Hz_neg_dp( self ):
        self.lossmap_freq_fb_100Hz( 0,  abs(self.rf_freq_stop), abs(self.rf_freq_step) )
        

if __name__ == '__main__':
    lm = offMomentumLossmap()
    #lm.lossmap_freq_loop_neg_dp()
    #lm.lossmap_freq_loop_pos_dp()
    #lm.lossmap_freq_fb_neg_dp()
    #lm.lossmap_freq_fb_neg_dp()
    #t = threading.Thread( target = lm.lossmap_freq_fb_neg_dp )
    #t.start()
    lm.stop_freq_trim()
    #lm.lossmap_freq_fb_pos_dp()
    
    #lm.lossmap_freq_fb_100Hz_neg_dp()
    #lm.lossmap_freq_fb_100Hz_pos_dp()

##Version to test wiht BEAM at injection

